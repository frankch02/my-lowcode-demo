import { Request, Response } from 'express';
import * as lowcodeData from './utils/lowcodeData'

export default {
  // 支持值为 Object 和 Array
  'GET /api/lowcode/moduleList': (req: Request, res: Response) => {
    lowcodeData.queryModuleList().then(data => {
      res.send({
        success: true,
        data,
      })
    }).catch(err => {
      res.status(500).send({
        data: {
          err,
        },
        errorCode: '500',
        errorMessage: '数据库错误：' + JSON.stringify(err),
        success: false,
      });
    })
  },
  // 支持值为 Object 和 Array
  'GET /api/lowcode/moduleInfo': (req: Request, res: Response) => {
    const { id } = req.query;
    lowcodeData.queryModule(Number(id)).then(data => {
      res.send({
        success: true,
        data,
      })
    }).catch(err => {
      res.status(500).send({
        data: {
          err,
        },
        errorCode: '500',
        errorMessage: '数据库错误：' + JSON.stringify(err),
        success: false,
      });
    })
  },

  //删除模块
  'GET /api/lowcode/deleteModule': (req: Request, res: Response) => {
    const { id } = req.query;
    lowcodeData.deleteModuleById(Number(id)).then(data => {
      res.send({
        success: true,
        data,
      })
    }).catch(err => {
      res.status(500).send({
        data: {
          err,
        },
        errorCode: '500',
        errorMessage: '数据库错误：' + JSON.stringify(err),
        success: false,
      });
    })
  },
  // 支持值为 Object 和 Array
  'POST /api/lowcode/updateModuleName': (req: Request, res: Response) => {
    const { id, name } = req.body;
    lowcodeData.updateModuleName(id, name).then(data => {
      res.send({
        success: true,
        data,
      })
    }).catch(err => {
      res.status(500).send({
        data: {
          err,
        },
        errorCode: '500',
        errorMessage: '数据库错误：' + JSON.stringify(err),
        success: false,
      });
    })
  },
  // 支持值为 Object 和 Array
  'POST /api/lowcode/updateModuleScheam': (req: Request, res: Response) => {
    const { id, packages, schema } = req.body;
    lowcodeData.updateModuleSchema(id, [schema, packages]).then(data => {
      res.send({
        success: true,
        data,
      })
    }).catch(err => {
      res.status(500).send({
        data: {
          err,
        },
        errorCode: '500',
        errorMessage: '数据库错误：' + JSON.stringify(err),
        success: false,
      });
    })
  },
  // 支持值为 Object 和 Array
  'POST /api/lowcode/addModule': (req: Request, res: Response) => {
    // const { id, name,assets } = req.body;
    lowcodeData.addModule(req.body).then(data => {
      res.send({
        success: true,
        data,
      })
    }).catch(err => {
      res.status(500).send({
        data: {
          err,
        },
        errorCode: '500',
        errorMessage: '数据库错误：' + JSON.stringify(err),
        success: false,
      });
    })
  },
  'POST /api/lowcode/moduleRelease': (req: Request, res: Response) => {
    const { id, vesion, remark } = req.body;
    lowcodeData.updateModuleRelease(id, [vesion, remark]).then(data => {
      res.send({
        success: true,
        data,
      })
    }).catch(err => {
      res.status(500).send({
        data: {
          err,
        },
        errorCode: '500',
        errorMessage: '数据库错误：' + JSON.stringify(err),
        success: false,
      });
    })
  },
};
