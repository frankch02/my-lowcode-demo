//import { execSql } from './mysqlDB';
const mysqlDB = require('./mysqlDB')
export const selectMenu = (data: any) => {
  let sql = 'select id,name,path,component,hideInMenu,layout,parent,icon,type,target,headerRender,footerRender,menuRender,menuHeaderRender,fixedHeader,fixSiderbar,navTheme,headerTheme from menu where 1=1';
  let values = []
  if (data.name) {
    sql += ' and name like ?'
    values.push(`%${data.name}%`)
  }

  return mysqlDB.execSql(sql, values);
}

export const insertMenu = (data: any) => {
  let values = [data.name, data.path, data.component, data.hideInMenu, data.parent, data.icon, data.type];
  let sql = 'insert into menu(name,path,component,hideInMenu,parent,icon,type) values(?)';
  return mysqlDB.execSql(sql, [values])
}

export const updateMenu = (data: any) => {
  let values = [data.name, data.path, data.component, data.hideInMenu, data.parent, data.icon, data.type, data.id];
  let sql = 'update menu set name=?,path=?,component=?,hideInMenu=?,parent=?,icon=?,type=? where id=?';
  return mysqlDB.execSql(sql, values)
}

export const deleteMenu = (id: number) => {
  let sql = 'delete from menu where id=?';
  return mysqlDB.execSql(sql, [id])
}