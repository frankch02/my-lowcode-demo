import { dynamic } from 'umi';

export function mergeRoutes(routes) {
  if (!Array.isArray(routes)) return [];
  return routes.map((route) => {
    const component = route.component;
    if (component) {
      route.component = dynamic({
        loader: async function () {
          // 这里的注释 webpackChunkName 可以指导 webpack 将该组件 HugeA 以这个名字单独拆出去
          const { default: module } = await import(`@/pages/${component}.tsx`);
          return module;
        },
      });
    }
    if (route.routes) {
      route.routes = mergeRoutes(route.routes);
    }
    return route;
  });
}
