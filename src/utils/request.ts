import type { RequestConfig } from 'umi';
const isDev = process.env.NODE_ENV === 'development';
const provBaseUrl = 'http://47.105.166.63:3000';
export const authHeaderInterceptor = function (url: string, options: RequestConfig) {
  //const authHeader = { Authorization: 'Bearer xxxxxx' };
  if (!options.headers) {
    options.headers = {};
  }
  if (options.noToken !== true) {
    options.headers.Authorization = 'Bearer xxxxxx';
  }
  const tempUrl = url.indexOf('/') == 0 ? url : '/' + url;
  return {
    url: isDev ? tempUrl : `${provBaseUrl}${tempUrl}`,
    //url: `http://120.27.22.173:7000${url}`,
    //url,
    options: { ...options, interceptors: true },
  };
};