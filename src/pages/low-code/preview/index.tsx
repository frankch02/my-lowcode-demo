import React, { useState, useEffect } from 'react';
import preview from './components/preview';
import './index.css';
import { history, useParams } from 'umi';
export default function () {
  const params = useParams();
  const data = { ...history.location.query, ...params };
  if (params.moduleId) {
    data.type = 'release';
  } else {
    data.type = 'preview';
  }
  useEffect(() => {
    preview(data);
  });
  return <div id="lowcode-preview" className="lowcode-preview"></div>;
}
