import pages from '@/locales/bn-BD/pages';
import type { ActionType, ProColumns, ProDescriptionsItemProps } from '@ant-design/pro-components';
import { FormattedMessage } from 'umi';
import { Tag, Popconfirm } from 'antd';
export function getColumns(pageData: any): ProColumns[] {
  return [
    {
      title: 'ID',
      dataIndex: 'id',
      search: false,
    },
    {
      title: '模块名称',
      dataIndex: 'name',
    },
    {
      title: '组件库',
      dataIndex: 'assets',
      search: false,
    },
    {
      title: '状态',
      dataIndex: 'state',
      search: false,
      render: (text, record) => {
        switch (text) {
          case '1':
            return <Tag color="red">未发布</Tag>;
          case '2':
            return <Tag color="green">已发布</Tag>;
          case '3':
            return <Tag color="orange">待发布</Tag>;
          default:
            return '';
        }
      },
    },
    {
      title: '模块路由',
      dataIndex: 'name',
      search: false,
      render: (_, record: any) => {
        if (record.state != 1) {
          return `/module/${record.id}`;
        } else {
          return '';
        }
      },
    },
    {
      title: '版本',
      dataIndex: 'vesion',
      search: false,
    },
    {
      title: '版本说明',
      dataIndex: 'vesion_remark',
      search: false,
    },
    {
      title: '发布时间',
      dataIndex: 'release_time',
      search: false,
    },
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      search: false,
      render: (_, record: any) => {
        const btns = [];
        btns.push(
          <a
            key={'edit'}
            onClick={() => {
              pageData.setCurrentRow(record);
              pageData.handleModalVisible(true);
            }}
          >
            <FormattedMessage id={'pages.data.edit'} defaultMessage="编辑" />
          </a>,
        );
        btns.push(
          <a
            key={'desing'}
            onClick={() => {
              window.open(`/moduleDesign?moduleId=${record.id}`);
            }}
          >
            <FormattedMessage id={'pages.data.design'} defaultMessage="设计" />
          </a>,
        );
        if (record.schema && record.packages) {
          btns.push(
            <a
              key={'preview'}
              onClick={() => {
                window.open(`/preview?moduleId=${record.id}`);
              }}
            >
              <FormattedMessage id={'pages.data.preview'} defaultMessage="预览" />
            </a>,
          );
        }
        if (record.schema && record.packages && record.state != '2') {
          btns.push(
            <a
              key={'release'}
              onClick={() => {
                pageData.handleReleaseFormVisible(true);
                pageData.setReleseRow(record);
              }}
            >
              <FormattedMessage id={'pages.data.release'} defaultMessage="发布" />
            </a>,
          );
        }
        if (record.state == '1') {
          btns.push(
            <Popconfirm
              key={'delete'}
              title="确定要删除模块吗？"
              onConfirm={() => {
                pageData.deleteRow(record.id);
              }}
              okText="确定"
              cancelText="取消"
            >
              <a>
                <FormattedMessage id={'pages.data.delete'} defaultMessage="删除" />
              </a>
            </Popconfirm>,
          );
        }
        return btns;
      },
    },
  ];
}
