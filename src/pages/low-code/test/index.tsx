// 注意: 出码引擎注入的临时变量默认都以 "__$$" 开头，禁止在搭建的代码中直接访问。
// 例外：react 框架的导出名和各种组件名除外。
import React from 'react';

import {
  Form,
  Input,
  InputNumber,
  Select,
  Button,
  Icon,
  Table,
} from '@alilc/antd-lowcode-materials';

import { createFetchHandler as __$$createFetchRequestHandler } from '@alilc/lowcode-datasource-fetch-handler';

import { create as __$$createDataSourceEngine } from '@alilc/lowcode-datasource-engine/runtime';

import utils, { RefsManager } from '@/low-code/utils';

import * as __$$i18n from '@/low-code/i18n';

import __$$constants from '@/low-code/constants';

import './index.css';

class $$Page extends React.Component {
  _context = this;

  _dataSourceConfig = this._defineDataSourceConfig();
  _dataSourceEngine = __$$createDataSourceEngine(this._dataSourceConfig, this, {
    runtimeConfig: true,
    requestHandlersMap: { fetch: __$$createFetchRequestHandler() },
  });

  get dataSourceMap() {
    return this._dataSourceEngine.dataSourceMap || {};
  }

  reloadDataSource = async () => {
    await this._dataSourceEngine.reloadDataSource();
  };

  get constants() {
    return __$$constants || {};
  }

  constructor(props, context) {
    super(props);

    this.utils = utils;

    this._refsManager = new RefsManager();

    __$$i18n._inject2(this);

    this.state = { text: 'outer', isShowDialog: false };
  }

  $ = (refName) => {
    return this._refsManager.get(refName);
  };

  $$ = (refName) => {
    return this._refsManager.getAll(refName);
  };

  _defineDataSourceConfig() {
    const _this = this;
    return {
      list: [
        {
          type: 'fetch',
          isInit: function () {
            return true;
          }.bind(_this),
          options: function () {
            return {
              params: {},
              method: 'GET',
              isCors: true,
              timeout: 5000,
              headers: {},
              uri: 'mock/info.json',
            };
          }.bind(_this),
          id: 'info',
          shouldFetch: function () {
            console.log('should fetch.....');
            return true;
          },
        },
      ],
    };
  }

  componentWillUnmount() {
    console.log('will unmount');
  }

  testFunc() {
    console.log('test func');
  }

  onClick() {
    this.setState({
      isShowDialog: true,
    });
  }

  closeDialog() {
    this.setState({
      isShowDialog: false,
    });
  }

  componentDidMount() {
    this._dataSourceEngine.reloadDataSource();

    console.log('did mount');
  }

  render() {
    const __$$context = this._context || this;
    const { state } = __$$context;
    return (
      <div ref={this._refsManager.linkRef('outerView')} style={{ height: '100%' }}>
        <Form
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 14 }}
          onValuesChange={function () {
            const self = this;
            try {
              return function onValuesChange(changedValues, allValues) {
                console.log('onValuesChange', changedValues, allValues);
              }.apply(self, arguments);
            } catch (e) {
              console.warn('call function which parsed by lowcode failed: ', e);
              return e.message;
            }
          }}
          onFinish={function () {
            const self = this;
            try {
              return function onFinish(values) {
                console.log('onFinish', values);
              }.apply(self, arguments);
            } catch (e) {
              console.warn('call function which parsed by lowcode failed: ', e);
              return e.message;
            }
          }}
          onFinishFailed={function () {
            const self = this;
            try {
              return function onFinishFailed({ values, errorFields, outOfDate }) {
                console.log('onFinishFailed', values, errorFields, outOfDate);
              }.apply(self, arguments);
            } catch (e) {
              console.warn('call function which parsed by lowcode failed: ', e);
              return e.message;
            }
          }}
          name="basic"
          ref={this._refsManager.linkRef('form_66ja')}
          colon={true}
          hideRequiredMark={false}
          layout="inline"
          preserve={true}
          scrollToFirstError={true}
          validateMessages={{ required: "'${name}' 不能为空" }}
          style={{ padding: '10px' }}
        >
          <Form.Item
            label="表单项"
            labelAlign="right"
            colon={true}
            required={false}
            noStyle={false}
            valuePropName="value"
            name="a"
            requiredobj={{ required: false, message: '必填' }}
            typeobj={{ message: '' }}
            lenobj={{ max: 0, min: 0, message: '' }}
            patternobj={{ pattern: '', message: '' }}
          >
            <Input placeholder="请输入" bordered={true} disabled={false} size="middle" />
          </Form.Item>
          <Form.Item
            label="表单项"
            labelAlign="right"
            colon={true}
            required={false}
            noStyle={false}
            valuePropName="value"
            name="b"
            typeobj={{ message: '' }}
            lenobj={{ max: 0, min: 0, message: '' }}
            patternobj={{ pattern: '', message: '' }}
            style={{ width: '220px' }}
          >
            <InputNumber
              placeholder="请输入"
              autoFocus={false}
              disabled={false}
              controls={true}
              bordered={true}
              size="middle"
            />
          </Form.Item>
          <Form.Item
            label="表单项"
            name="e"
            labelAlign="right"
            colon={true}
            required={false}
            noStyle={false}
            valuePropName="value"
            typeobj={{ message: '' }}
            lenobj={{ max: 0, min: 0, message: '' }}
            patternobj={{ pattern: '', message: '' }}
            ref={this._refsManager.linkRef('form.item-b1d54ff6')}
          >
            <Select
              style={{ width: 200 }}
              options={[
                { label: 'A', value: 'A' },
                { label: 'B', value: 'B' },
                { label: 'C', value: 'C' },
              ]}
              allowClear={false}
              autoFocus={false}
              defaultActiveFirstOption={true}
              disabled={false}
              labelInValue={false}
              showSearch={false}
              size="middle"
              loading={false}
              bordered={true}
              filterOption={true}
              optionFilterProp="value"
              tokenSeparators={[]}
              maxTagCount={0}
              maxTagTextLength={0}
              ref={this._refsManager.linkRef('select-2c1c4881')}
            />
          </Form.Item>
          <Button
            type="primary"
            htmlType="button"
            size="middle"
            shape="default"
            block={false}
            danger={false}
            ghost={false}
            disabled={false}
            style={{ marginLeft: '20px' }}
          >
            搜索
          </Button>
          {!!false && (
            <Form.Item
              wrapperCol={{ offset: 6 }}
              labelAlign="left"
              colon={true}
              required={false}
              noStyle={false}
              valuePropName="value"
              typeobj={{ message: '' }}
              lenobj={{ max: 0, min: 0, message: '' }}
              patternobj={{ pattern: '', message: '' }}
              style={{ display: 'flex', flexDirection: 'row', width: '220px' }}
              ref={this._refsManager.linkRef('form.item-4bcb6fc9')}
            >
              <Button
                type="primary"
                htmlType="submit"
                size="middle"
                shape="default"
                icon={<Icon type="SmileOutlined" size={20} rotate={0} spin={false} />}
                block={false}
                danger={false}
                ghost={false}
                disabled={false}
              >
                提交
              </Button>
              <Button
                style={{ marginLeft: 20 }}
                htmlType="button"
                size="middle"
                shape="default"
                icon={<Icon type="SmileOutlined" size={20} rotate={0} spin={false} />}
                block={false}
                danger={false}
                ghost={false}
                disabled={false}
              >
                取消
              </Button>
            </Form.Item>
          )}
        </Form>
        <Table
          dataSource={[
            { id: '1', name: '胡彦斌', age: 32, address: '西湖区湖底公园1号' },
            { id: '2', name: '王一博', age: 28, address: '滨江区网商路699号' },
          ]}
          columns={[
            { title: '姓名', dataIndex: 'name', key: 'name' },
            { title: '年龄', dataIndex: 'age', key: 'age' },
          ]}
          rowKey="id"
          pagination={{
            pageSize: 10,
            total: 15,
            current: 1,
            showSizeChanger: false,
            showQuickJumper: false,
            simple: false,
            size: 'default',
          }}
          loading={false}
          showHeader={true}
          size="default"
          tableLayout=""
          scroll={{ scrollToFirstRowOnChange: true }}
          style={{ marginTop: '20px' }}
        />
      </div>
    );
  }
}

export default $$Page;

function __$$eval(expr) {
  try {
    return expr();
  } catch (error) {}
}

function __$$evalArray(expr) {
  const res = __$$eval(expr);
  return Array.isArray(res) ? res : [];
}

function __$$createChildContext(oldContext, ext) {
  const childContext = {
    ...oldContext,
    ...ext,
  };
  childContext.__proto__ = oldContext;
  return childContext;
}
