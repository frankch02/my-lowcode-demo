import React, { useState, useEffect } from 'react';
import { init, plugins } from '@alilc/lowcode-engine';
import { createFetchHandler } from '@alilc/lowcode-datasource-fetch-handler';
import EditorInitPlugin from './plugins/plugin-editor-init';
import UndoRedoPlugin from '@alilc/lowcode-plugin-undo-redo';
import ZhEnPlugin from '@alilc/lowcode-plugin-zh-en';
import CodeGenPlugin from '@alilc/lowcode-plugin-code-generator';
import DataSourcePanePlugin from '@alilc/lowcode-plugin-datasource-pane';
import SchemaPlugin from '@alilc/lowcode-plugin-schema';
import CodeEditorPlugin from '@alilc/lowcode-plugin-code-editor';
import ManualPlugin from '@alilc/lowcode-plugin-manual';
import InjectPlugin from '@alilc/lowcode-plugin-inject';
import SimulatorResizerPlugin from '@alilc/lowcode-plugin-simulator-select';
import ComponentPanelPlugin from '@alilc/lowcode-plugin-components-pane';
import DefaultSettersRegistryPlugin from './plugins/plugin-default-setters-registry';
import LoadIncrementalAssetsWidgetPlugin from './plugins/plugin-load-incremental-assets-widget';
import SaveSamplePlugin from './plugins/plugin-save-sample';
import PreviewSamplePlugin from './plugins/plugin-preview-sample';
import CustomSetterSamplePlugin from './plugins/plugin-custom-setter-sample';
import SetRefPropPlugin from '@alilc/lowcode-plugin-set-ref-prop';
import LogoSamplePlugin from './plugins/plugin-logo-sample';
import './index.scss';
import { createUmiRequestHandler } from '@/low-code/request';
import { useParams, useLocation } from 'umi';
import utils from '@/low-code/utils';
import constants from '@/low-code/constants';

async function registerPlugins(params: any) {
  await plugins.register(InjectPlugin, { override: true });

  await plugins.delete(EditorInitPlugin.pluginName);
  await plugins.register(EditorInitPlugin, {
    scenarioName: 'module-design',
    displayName: '模块设计',
    moduleId: params.moduleId,
  });

  // 设置内置 setter 和事件绑定、插件绑定面板
  await plugins.register(DefaultSettersRegistryPlugin, { override: true });

  //await plugins.register(LogoSamplePlugin, { override: true });

  await plugins.register(ComponentPanelPlugin, { override: true });

  await plugins.register(SchemaPlugin, { override: true });

  await plugins.register(ManualPlugin, { override: true });
  // 注册回退/前进
  await plugins.register(UndoRedoPlugin, { override: true });

  // 注册中英文切换
  await plugins.register(ZhEnPlugin, { override: true });

  await plugins.register(SetRefPropPlugin, { override: true });

  await plugins.register(SimulatorResizerPlugin, { override: true });

  //await plugins.register(LoadIncrementalAssetsWidgetPlugin, { override: true });

  // 插件参数声明 & 传递，参考：https://lowcode-engine.cn/site/docs/api/plugins#设置插件参数版本示例
  await plugins.delete(DataSourcePanePlugin.pluginName);
  await plugins.register(DataSourcePanePlugin, {
    importPlugins: [],
    dataSourceTypes: [
      {
        type: 'fetch',
      },
      {
        type: 'jsonp',
      },
    ],
  });

  await plugins.register(CodeEditorPlugin, { override: true });

  // 注册出码插件
  await plugins.register(CodeGenPlugin, { override: true });

  await plugins.register(SaveSamplePlugin, { override: true });

  await plugins.register(PreviewSamplePlugin, { override: true });

  await plugins.register(CustomSetterSamplePlugin, { override: true });
}

async function initLowCodeEditor(params: any) {
  await registerPlugins(params);
  init(document.getElementById('lowcode-module-design') || undefined, {
    locale: 'zh-CN',
    enableCondition: true,
    enableCanvasLock: true,
    // 默认绑定变量
    supportVariableGlobally: true,
    requestHandlersMap: {
      //fetch: createFetchHandler(),
      fetch: createUmiRequestHandler(),
    },
    appHelper: {
      utils,
      constants,
    },
  });
}
export default () => {
  const location = useLocation();
  useEffect(() => {
    initLowCodeEditor(location.query);
  }, []);
  return <div id="lowcode-module-design"></div>;
};
