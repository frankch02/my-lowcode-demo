import { IPublicModelPluginContext } from '@alilc/lowcode-types';
import { Button } from '@alifd/next';
import { saveSchema } from '../../services/schemaData';
import { message } from 'antd';

// 保存功能示例
const SaveSamplePlugin = (ctx: IPublicModelPluginContext) => {
  return {
    async init() {
      const { skeleton, hotkey, config } = ctx;
      const moduleId = config.get('moduleId');
      skeleton.add({
        name: 'saveSample',
        area: 'topArea',
        type: 'Widget',
        props: {
          align: 'right',
        },
        content: <Button onClick={() => save(moduleId)}>保存</Button>,
      });
      hotkey.bind('command+s', async (e) => {
        e.preventDefault();
        save(moduleId);
      });
    },
  };
};
SaveSamplePlugin.pluginName = 'SaveSamplePlugin';
SaveSamplePlugin.meta = {
  dependencies: ['EditorInitPlugin'],
};

async function save(moduleId: number) {
  await saveSchema(moduleId);
  message.success('保存成功！');
}
export default SaveSamplePlugin;
