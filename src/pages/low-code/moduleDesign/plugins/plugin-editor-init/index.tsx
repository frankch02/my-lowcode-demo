import { IPublicModelPluginContext } from '@alilc/lowcode-types';
import { injectAssets } from '@alilc/lowcode-plugin-inject';
import assetsAntd from '@/low-code/services/assets-antd.json';
import defaultSchema from '../../services/defaultSchema.json';
import { getPageSchema, getModuleData } from '../../services/schemaData';

const EditorInitPlugin = (ctx: IPublicModelPluginContext, options: any) => {
  return {
    async init() {
      const { material, project, config } = ctx;
      const scenarioName = options['scenarioName'];
      const scenarioDisplayName = options['displayName'] || scenarioName;
      const scenarioInfo = options['info'] || {};
      // 保存在config中用于引擎范围其他插件使用
      config.set('scenarioName', scenarioName);
      config.set('scenarioDisplayName', scenarioDisplayName);
      //config.set('scenarioInfo', scenarioInfo);

      config.set('moduleId', options.moduleId);

      const moduleData = await getModuleData(options.moduleId);
      const assets = require(`@/low-code/assets/${moduleData.assets}.json`);
      // 设置物料描述
      await material.setAssets(await injectAssets(assets));
      const schema = moduleData.schema?.componentsTree?.[0];
      if (schema) {
        // 加载 schema
        project.openDocument(schema);
      } else {
        project.openDocument(defaultSchema as any);
      }
    },
  };
};
EditorInitPlugin.pluginName = 'EditorInitPlugin';
EditorInitPlugin.meta = {
  preferenceDeclaration: {
    title: '保存插件配置',
    properties: [
      {
        key: 'scenarioName',
        type: 'string',
        description: '用于localstorage存储key',
      },
      {
        key: 'displayName',
        type: 'string',
        description: '用于显示的场景名',
      },
      {
        key: 'moduleId',
        type: 'string',
        description: '模块id',
      },
    ],
  },
};
export default EditorInitPlugin;
