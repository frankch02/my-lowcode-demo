import type { ActionType, ProColumns, ProDescriptionsItemProps } from '@ant-design/pro-components';
export const tableList = [
  {
    id: '1',
    name: '测试模块1',
  },
  {
    id: '2',
    name: '测试模块2',
  },
];
export const columns: ProColumns[] = [
  {
    title: 'ID',
    dataIndex: 'id',
  },
  {
    title: '模块名称',
    dataIndex: 'name',
  },
  {
    title: '操作',
    dataIndex: 'option',
    render: (_, record) => [
      <a key="config" onClick={() => {}}>
        编辑
      </a>,
      <a key="preview" onClick={() => {}}>
        预览
      </a>,
    ],
  },
];

const schemaList = [
  {
    id: '1',
    schema: {
      version: '1.0.0',
      componentsMap: [
        {
          package: '@alilc/antd-lowcode-materials',
          version: '1.2.1',
          exportName: 'Table',
          main: '',
          destructuring: true,
          componentName: 'Table',
        },
        { devMode: 'lowCode', componentName: 'Page' },
      ],
      componentsTree: [
        {
          componentName: 'Page',
          id: 'node_dockcviv8fo1',
          props: { ref: 'outerView', style: { height: '100%' } },
          fileName: '/',
          dataSource: {
            list: [
              {
                type: 'fetch',
                isInit: true,
                options: {
                  params: {},
                  method: 'GET',
                  isCors: true,
                  timeout: 5000,
                  headers: {},
                  uri: 'mock/info.json',
                },
                id: 'info',
                shouldFetch: {
                  type: 'JSFunction',
                  value: "function() { \n  console.log('should fetch.....');\n  return true; \n}",
                },
              },
            ],
          },
          state: {
            text: { type: 'JSExpression', value: '"outer"' },
            isShowDialog: { type: 'JSExpression', value: 'false' },
          },
          css: 'body {\n  font-size: 12px;\n}\n\n.button {\n  width: 100px;\n  color: #ff00ff\n}',
          lifeCycles: {
            componentDidMount: {
              type: 'JSFunction',
              value: "function componentDidMount() {\n  console.log('did mount');\n}",
            },
            componentWillUnmount: {
              type: 'JSFunction',
              value: "function componentWillUnmount() {\n  console.log('will unmount');\n}",
            },
          },
          methods: {
            testFunc: {
              type: 'JSFunction',
              value: "function testFunc() {\n  console.log('test func');\n}",
            },
            onClick: {
              type: 'JSFunction',
              value: 'function onClick() {\n  this.setState({\n    isShowDialog: true\n  });\n}',
            },
            closeDialog: {
              type: 'JSFunction',
              value:
                'function closeDialog() {\n  this.setState({\n    isShowDialog: false\n  });\n}',
            },
          },
          originCode:
            'class LowcodeComponent extends Component {\n  state = {\n    "text": "outer",\n    "isShowDialog": false\n  }\n  componentDidMount() {\n    console.log(\'did mount\');\n  }\n  componentWillUnmount() {\n    console.log(\'will unmount\');\n  }\n  testFunc() {\n    console.log(\'test func\');\n  }\n  onClick() {\n    this.setState({\n      isShowDialog: true\n    })\n  }\n  closeDialog() {\n    this.setState({\n      isShowDialog: false\n    })\n  }\n}',
          hidden: false,
          title: '',
          isLocked: false,
          condition: true,
          conditionGroup: '',
          children: [
            {
              componentName: 'Table',
              id: 'node_oclqm4q3v91',
              props: {
                dataSource: [
                  { id: '1', name: '胡彦斌', age: 32, address: '西湖区湖底公园1号' },
                  { id: '2', name: '王一博', age: 28, address: '滨江区网商路699号' },
                ],
                columns: [
                  { title: '姓名', dataIndex: 'name', key: 'name' },
                  { title: '年龄', dataIndex: 'age', key: 'age' },
                ],
                rowKey: 'id',
                pagination: {
                  pageSize: 10,
                  total: 15,
                  current: 1,
                  showSizeChanger: false,
                  showQuickJumper: false,
                  simple: false,
                  size: 'default',
                },
                loading: false,
                showHeader: true,
                size: 'default',
                tableLayout: '',
                scroll: { scrollToFirstRowOnChange: true },
              },
              hidden: false,
              title: '',
              isLocked: false,
              condition: true,
              conditionGroup: '',
            },
          ],
        },
      ],
      i18n: {},
    },
    packages: [
      {
        package: 'moment',
        version: '2.24.0',
        urls: ['https://g.alicdn.com/mylib/moment/2.24.0/min/moment.min.js'],
        library: 'moment',
      },
      {
        package: 'lodash',
        library: '_',
        urls: ['https://g.alicdn.com/platform/c/lodash/4.6.1/lodash.min.js'],
      },
      { package: 'iconfont-icons', urls: '//at.alicdn.com/t/font_2369445_ukrtsovd92r.js' },
      {
        package: '@ant-design/icons',
        version: '4.7.0',
        urls: ['//g.alicdn.com/code/npm/@ali/ant-design-icons-cdn/4.5.0/index.umd.min.js'],
        library: 'icons',
      },
      {
        package: 'antd',
        version: '4.19.5',
        urls: [
          '//g.alicdn.com/code/lib/antd/4.23.0/antd.min.js',
          '//g.alicdn.com/code/lib/antd/4.23.0/antd.min.css',
        ],
        library: 'antd',
      },
      {
        title: 'fusion组件库',
        package: '@alifd/next',
        version: '1.26.4',
        urls: [
          'https://g.alicdn.com/code/lib/alifd__next/1.26.4/next.min.css',
          'https://g.alicdn.com/code/lib/alifd__next/1.26.4/next-with-locales.min.js',
        ],
        library: 'Next',
      },
      {
        package: '@alilc/antd-lowcode-materials',
        version: '1.2.1',
        library: 'AntdLowcode',
        urls: [
          'https://alifd.alicdn.com/npm/@alilc/antd-lowcode-materials@1.2.1/build/lowcode/view.js',
          'https://alifd.alicdn.com/npm/@alilc/antd-lowcode-materials@1.2.1/build/lowcode/view.css',
        ],
        editUrls: [
          'https://alifd.alicdn.com/npm/@alilc/antd-lowcode-materials@1.2.1/build/lowcode/view.js',
          'https://alifd.alicdn.com/npm/@alilc/antd-lowcode-materials@1.2.1/build/lowcode/view.css',
        ],
      },
    ],
  },
];

export function getSchemaById(id) {
  return schemaList.find((item) => item.id == id);
}
