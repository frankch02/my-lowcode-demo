// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

export async function getMenuList(options?: { [key: string]: any }) {
  return request('/api/menuList', {
    method: 'GET',
    ...(options || {}),
  });
}

export async function addMenu(data: any, options?: { [key: string]: any }) {
  return request('/api/addMenu', {
    method: 'POST',
    data,
    ...(options || {}),
  });
}

export async function editMenu(data: any, options?: { [key: string]: any }) {
  return request('/api/editMenu', {
    method: 'POST',
    data,
    ...(options || {}),
  });
}

export async function deleteMenu(params: any, options?: { [key: string]: any }) {
  return request('/api/deleteMenu', {
    method: 'GET',
    params,
    ...(options || {}),
  });
}

export async function getLowcodeModuleList(options?: { [key: string]: any }) {
  return request('/api/lowcode/moduleList', {
    method: 'GET',
    ...(options || {}),
  });
}

export async function getLowcodeModuleById(id: number, options?: { [key: string]: any }) {
  return request('/api/lowcode/moduleInfo', {
    method: 'GET',
    params: { id: id },
    ...(options || {}),
  });
}

export async function addLowcodeModule(data: any, options?: { [key: string]: any }) {
  return request('/api/lowcode/addModule', {
    method: 'POST',
    /* headers: {
      'Content-Type': 'application/json',
    }, */
    data,
    ...(options || {}),
  });
}

export async function updateLowcodeModuleScheam(data: any, options?: { [key: string]: any }) {
  return request('/api/lowcode/updateModuleScheam', {
    method: 'POST',
    /* headers: {
      'Content-Type': 'application/json',
    }, */
    data,
    ...(options || {}),
  });
}

export async function moduleRelease(data: any, options?: { [key: string]: any }) {
  return request('/api/lowcode/moduleRelease', {
    method: 'POST',
    /* headers: {
      'Content-Type': 'application/json',
    }, */
    data,
    ...(options || {}),
  });
}

export async function updateLowcodeModuleName(data: any, options?: { [key: string]: any }) {
  return request('/api/lowcode/updateModuleName', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data,
    ...(options || {}),
  });
}

export async function deleteModuleById(id: number, options?: { [key: string]: any }) {
  return request('/api/lowcode/deleteModule', {
    method: 'GET',
    params: { id: id },
    ...(options || {}),
  });
}
