// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/** 获取菜单 */
export async function getMenu(options?: { [key: string]: any }) {
  return request('/testData/menu.json', {
    method: 'GET',
  });
}
