本项目为 ant design pro5 集成阿里低代码开源引擎（lowcode-engine）的演示项目，包含低代码引擎集成，动态菜单，低代码模块管理等功能演示。

本项目采用本地 mock 服务作为后端服务，在后端服务中访问数据库进行操作

启动前，先将数据库脚本导入mysql数据库，并修改mock/utils/mysqlDB.ts中的数据库连接信息。

npm run start 启动演示项目
