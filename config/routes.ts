﻿export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        name: 'login',
        path: '/user/login',
        component: './user/Login',
      },
      {
        name: 'test',
        path: '/user/test',
        component: './user/Login/test.tsx',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/welcome',
    name: 'welcome',
    icon: 'smile',
    access: 'welcome',
    component: './Welcome',
  },
  {
    name: '模块预览',
    icon: 'table',
    layout: false,
    hideInMenu: true,
    target: '_blank',
    path: '/preview',
    component: './low-code/preview',
  },
  {
    name: '模块展示',
    icon: 'table',
    hideInMenu: true,
    path: '/module/:moduleId',
    component: './low-code/preview',
  },
  {
    name: '模块设计',
    icon: 'table',
    layout: false,
    hideInMenu: true,
    target: '_blank',
    path: '/moduleDesign',
    component: './low-code/moduleDesign',
  },
  {
    path: '/',
    redirect: '/welcome',
  },
  {
    component: './404',
  },
  /*  {
     path: '/admin',
     name: 'admin',
     icon: 'crown',
     access: 'canAdmin',
     routes: [
       {
         path: '/admin/sub-page',
         name: 'sub-page',
         icon: 'smile',
         component: './Welcome',
       },
       {
         component: './404',
       },
     ],
   },
   {
     name: 'list.table-list',
     icon: 'table',
     path: '/list',
     component: './TableList',
   },
   {
     name: '低代码',
     icon: 'table',
     path: '/low-code',
     routes: [{
       name: '编辑器',
       icon: 'table',
       path: '/low-code/editor',
       component: './low-code/editor',
     }, {
       name: '模块',
       icon: 'table',
       path: '/low-code/moduleList',
       component: './low-code/moduleList',
     }, {
       name: 'test1',
       icon: 'table',
       path: '/low-code/test',
       component: './low-code/test',
     }, {
       name: 'test2',
       icon: 'table',
       path: '/low-code/test2',
       component: './low-code/test2',
     }, {
       name: '预览',
       icon: 'table',
       layout: false,
       path: '/low-code/preview',
       component: './low-code/preview',
     }]
   }, */
];
